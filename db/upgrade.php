<?php

// This file keeps track of upgrades to 
// the programming module
//
// Sometimes, changes between versions involve
// alterations to database structures and other
// major things that may break installations.
//
// The upgrade function in this file will attempt
// to perform all the necessary actions to upgrade
// your older installtion to the current version.
//
// If there's something it cannot do itself, it
// will tell you what you need to do.
//
// The commands in here will all be database-neutral,
// using the functions defined in lib/ddllib.php

function xmldb_programming_upgrade($oldversion=0) {

    global $CFG, $THEME, $DB;

    $result = true;
    $dbman = $DB->get_manager();
/// And upgrade begins here. For each one, you'll need one 
/// block of code similar to the next one. Please, delete 
/// this comment lines once this file start handling proper
/// upgrade code.

/// if ($result && $oldversion < YYYYMMDD00) { //New version in version.php
///     $result = result of "/lib/ddllib.php" function calls
/// }
//2012051101
    //2011062402 is the latest version fits for moodle1.9x
    if ($result && $oldversion <= 2011062402) {
        $prefix = $DB->get_prefix();
        $sql = "ALTER TABLE `{$prefix}programming` CHANGE  COLUMN  `description` `intro` longtext NOT NULL,";
        $sql .= "CHANGE COLUMN `descformat` `introformat`  tinyint(2) NOT NULL DEFAULT 0;";
        $DB->change_database_structure($sql);
        upgrade_mod_savepoint(true, 2011062402, 'programming');
        
    }

    if ($result && $oldversion < 2012081804) {
        $prefix = $DB->get_prefix();
        $sql = "ALTER TABLE {$prefix}programming_presetcode CHANGE  COLUMN presetcodeforcheck presetcodeforcheck LONGTEXT NULL;";
        $DB->change_database_structure($sql);
        upgrade_mod_savepoint(true, 2012081804, 'programming');
    }
//2012080803
    if ($result && $oldversion < 2012081903) {
        $prefix = $DB->get_prefix();
        $sql = "ALTER TABLE {$prefix}programming_datafile CHANGE COLUMN checkdatasize checkdatasize bigint(10) NULL, CHANGE COLUMN checkdata checkdata longblob NULL;";
        $DB->change_database_structure($sql);
        upgrade_mod_savepoint(true, 2012081903, 'programming');
    }

    if ($result && $oldversion < 2013082801) {

        // Define field completionac to be added to programming.
        $table = new xmldb_table('programming');
        $field = new xmldb_field('completionac', XMLDB_TYPE_INTEGER, '2', null, XMLDB_NOTNULL, null, '0', 'timemodified');

        // Conditionally launch add field completionac.
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        // Programming savepoint reached.
        upgrade_mod_savepoint(true, 2013082801, 'programming');
    }

    if ($result && $oldversion < 2014051501) {

        // Define field completionac to be added to programming.
        $table = new xmldb_table('programming_languages');
        $field = new xmldb_field('visible', XMLDB_TYPE_INTEGER, '1', null, XMLDB_NOTNULL, null, '1', 'description');

        // Conditionally launch add field completionac.
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        $sql="insert into {programming_languages} (name, description, sourceext, headerext) VALUES ('gcc', 'C (GCC)', '.c', '.h')";
        $DB->execute($sql);
        $sql="insert into {programming_languages} (name, description, sourceext, headerext) VALUES ('g++', 'C++ (G++)', '.cpp .cxx', '.h .hpp')";
        $DB->execute($sql);
        $sql="insert into {programming_languages} (name, description, sourceext, headerext) VALUES ('java', 'Java (OpenJDK)', '.java', NULL)";
        $DB->execute($sql);
        $sql="insert into {programming_languages} (name, description, sourceext, headerext) VALUES ('fpc', 'Pascal (Free Pascal 2)', '.pas', NULL)";
        $DB->execute($sql);
        $sql="insert into {programming_languages} (name, description, sourceext, headerext) VALUES ('scilab', 'Scilab 5', '.sci', NULL)";
        $DB->execute($sql);
        $sql="insert into {programming_languages} (name, description, sourceext, headerext) VALUES ('python', 'Python 2', '.py', NULL)";
        $DB->execute($sql);
        $sql="insert into {programming_languages} (name, description, sourceext, headerext) VALUES ('dmcs', 'C# (Mono)', '.cs', NULL)";
        $DB->execute($sql);
        $sql="insert into {programming_languages} (name, description, sourceext, headerext) VALUES ('bash', 'Bash (Bash 3)', '.sh', NULL)";
        $DB->execute($sql);

        // Programming savepoint reached.
        upgrade_mod_savepoint(true, 2014051501, 'programming');
    }

    // base64_encode the 'code'
    if ($result && $oldversion < 2017021701) {
        // Define field completionac to be added to programming_submits.
        $table = new xmldb_table('programming_submits');
        $field = new xmldb_field('encodemode', XMLDB_TYPE_INTEGER, '2', null, XMLDB_NOTNULL, null, 0, 'code');

        // Conditionally launch add field completionac.
        if (!$dbman->field_exists($table, $field)) {
            $dbman->add_field($table, $field);
        }

        // Programming savepoint reached.
        upgrade_mod_savepoint(true, 2017021701, 'programming');
    }

    return $result;
}

?>
