<?php

// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

/**
 * This file replaces the legacy STATEMENTS section in db/install.xml,
 * lib.php/modulename_install() post installation hook and partially defaults.php
 *
 * @package    mod
 * @subpackage programming
 * @copyright  2011 Your Name <your@email.adress>
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 */

/**
 * Post installation procedure
 *
 * @see upgrade_plugins_modules()
 */
function xmldb_programming_install() {
    global $DB;
    $sql="insert into {programming_languages} (name, description, sourceext, headerext) VALUES ('gcc', 'C (GCC)', '.c', '.h')";
    $DB->execute($sql);
    $sql="insert into {programming_languages} (name, description, sourceext, headerext) VALUES ('g++', 'C++ (G++)', '.cpp .cxx', '.h .hpp')";
    $DB->execute($sql);
    $sql="insert into {programming_languages} (name, description, sourceext, headerext) VALUES ('java', 'Java (OpenJDK)', '.java', NULL)";
    $DB->execute($sql);
    $sql="insert into {programming_languages} (name, description, sourceext, headerext) VALUES ('fpc', 'Pascal (Free Pascal 2)', '.pas', NULL)";
    $DB->execute($sql);
    $sql="insert into {programming_languages} (name, description, sourceext, headerext) VALUES ('scilab', 'Scilab 5', '.sci', NULL)";
    $DB->execute($sql);
    $sql="insert into {programming_languages} (name, description, sourceext, headerext) VALUES ('python', 'Python 2', '.py', NULL)";
    $DB->execute($sql);
    $sql="insert into {programming_languages} (name, description, sourceext, headerext) VALUES ('dmcs', 'C# (Mono)', '.cs', NULL)";
    $DB->execute($sql);
    $sql="insert into {programming_languages} (name, description, sourceext, headerext) VALUES ('bash', 'Bash (Bash 3)', '.sh', NULL)";
    $DB->execute($sql);

    // $DB->set_field('modules', 'visible', 0, array('name'=>'programming'));
}

/**
 * Post installation recovery procedure
 *
 * @see upgrade_plugins_modules()
 */
function xmldb_programming_install_recovery() {
}
