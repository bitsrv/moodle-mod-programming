<?php

    require_once('../../config.php');
    require_once('lib.php');
    
    $id = optional_param('id', 0, PARAM_INT);     // programming ID
    $s1 = required_param('s1', PARAM_INT);
    $s2 = required_param('s2', PARAM_INT);

    if ($id) {
        if (! $cm = get_coursemodule_from_id('programming', $id)) {
            error('Course Module ID was incorrect');
        }
    
        if (! $course = $DB->get_record('course', array('id' => $cm->course))) {
            error('Course is misconfigured');
        }
    
        if (! $programming = $DB->get_record('programming', array('id' => $cm->instance))) {
            error('Course module is incorrect');
        }
    }

    require_login($course->id, true, $cm);
    $context = get_context_instance(CONTEXT_MODULE, $cm->id);

    require_capability('mod/programming:viewhistory', $context);

    $submit1 = $DB->get_record('programming_submits', array('id' => $s1));
    $submit2 = $DB->get_record('programming_submits', array('id' => $s2));

    if ($submit1->userid != $USER->id || $submit2->userid != $USER->id) {
        require_capability('mod/programming:viewotherprogram', $context);
    }

/// Print the page header
    $pagename = get_string('submithistory', 'programming');
    $CFG->scripts[] = '/mod/programming/js/dp/shCore.js';
    $CFG->scripts[] = '/mod/programming/js/dp/shBrushCSharp.js';
    $CFG->stylesheets[] = '/mod/programming/js/dp/SyntaxHighlighter.css';
    $params['id']=$id;
    $params['s1']=$s1;
    $params['s2']=$s2;
    $PAGE->set_url('/mod/programming/history_diff.php', $params);


    include_once('pageheader.php');

    $renderer = $PAGE->get_renderer('mod_programming');
    $tabs = programming_navtab('history', null, $course, $programming, $cm);
    echo $renderer->render_navtab($tabs);

    echo html_writer::tag('h2', $programming->name);
    if ($USER->id != $userid) {
        $u = $DB->get_record('user', array('id' => $userid));
        echo html_writer::tag('h3', get_string('viewsubmithistoryof', 'programming', fullname($u)));
    } else {
        echo html_writer::tag('h3', get_string('viewsubmithistory', 'programming'));
    }

/// Print tabs
    $currenttab = 'history';
    include_once('tabs.php');

/// Print page content

    ini_set("include_path", ".:./lib");
    require_once('Text/Diff.php');
    require_once('text_diff_render_html.php');



    $lines1 = explode("\n", $submit1->encodemode?base64_decode($submit1->code):$submit1->code);
    $lines2 = explode("\n", $submit2->encodemode?base64_decode($submit2->code):$submit2->code);

    $diff = new Text_Diff('auto', array($lines1, $lines2));

    $renderer = new Text_Diff_Renderer_html();

    echo '<pre>';
    echo $renderer->render($diff);
    echo '</pre>';

/// Finish the page
    // print_footer($course);
    echo $OUTPUT->footer($course);

?>
