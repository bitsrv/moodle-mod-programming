<?PHP
    require_once("../../config.php");
    require_once("lib.php");
    require_once("../../lib/filelib.php");

    $id = required_param('id', PARAM_INT);     // programming ID
    $group = optional_param('group', 0, PARAM_INT);

    $params = array('id' => $id);
    if (!empty($group)) {
        $params['group'] = $group;
    }
    $PAGE->set_url('/mod/programming/packaing.php', $params);

    if (! $cm = get_coursemodule_from_id('programming', $id)) {
        print_error('invalidcoursemodule');
    }

    if (! $course = $DB->get_record('course', array('id' => $cm->course))) {
        print_error('coursemisconf');
    }

    if (! $programming = $DB->get_record('programming', array('id' => $cm->instance))) {
        print_error('invalidprogrammingid', 'programming');
    }

    $context = get_context_instance(CONTEXT_MODULE, $cm->id);
    // $context = context_moe_programming::instance($userid)->id;
    require_login($course->id, true, $cm);
    require_capability('mod/programming:viewotherprogram', $context);

    add_to_log($course->id, 'programming', 'package', me(), $programming->id);

    $users = array(); 
    if ($group != 0) {
        $users = groups_get_members($group,'u.id');
    } else {
        $mygroupid = groups_get_all_groups($course->id);
        if ($mygroupid) {
            foreach ($mygroupid as $grp){
                $users = $users + groups_get_members($grp->id,'u.id');
            }
        } else {
            $users = False;
        }
    }

    $sql = "SELECT * FROM {programming_submits} WHERE programmingid={$programming->id}";
    if (is_array($users)) {
        $sql .= ' AND userid IN ('.implode(',', array_keys($users)).')';
    }
    $sql .= ' ORDER BY timemodified DESC';
    $submits = $DB->get_records_sql($sql);
    $users = array();
    $latestsubmits = array();
    if (is_array($submits)) {
        foreach ($submits as $submit) {
            if (in_array($submit->userid, $users)) continue;
            $users[] = $submit->userid;
            $latestsubmits[] = $submit;
        }
    }
    $sql = 'SELECT * FROM {user} WHERE id IN ('.implode(',', $users).')';
    $users = $DB->get_records_sql($sql);

    // create dir
    $dirname = $CFG->dataroot.'/temp';
    if (!file_exists($dirname)) {
        mkdir($dirname, 0777) or ('Failed to create dir');
    }
    $dirname .= '/programming';
    if (!file_exists($dirname)) {
        mkdir($dirname, 0777) or ('Failed to create dir');
    }

    $dirname .='/'.$programming->id;

    if (file_exists($dirname)) {
        if (is_dir($dirname)) {
            fulldelete($dirname) or error('Failed to remove dir contents');
            //rmdir($dirname) or error('Failed to remove dir');
        } else {
            unlink($dirname) or error('Failed to delete file');
        }
    }
    mkdir($dirname, 0700) or error('Failed to create dir');

    $files = array();
    // write files
    foreach ($latestsubmits as $submit) {
        if ($submit->language == 1) $ext = '.c';
        elseif ($submit->language == 2) $ext = '.cxx';
        $filename = $dirname.'/'.$users[$submit->userid]->username.'-'.$submit->id.$ext;
        $files[] = $filename;
        $f = fopen($filename, 'w');
        fwrite($f, $submit->encodemode?base64_decode($submit->code):$submit->code);
        fwrite($f, "\r\n");
        fclose($f);
    }
    // zip file name
    $zipfilename = '/programming-'.$programming->id;
    if ($group != 0) {
        $zipfilename .= '-'.groups_get_group_name($group);
    } else {
        $zipfilename .= '-all';
    }
    $zipfilename .= '.zip';

    // zip file destination, zip file
    $dest = $dirname.$zipfilename;
    if (file_exists($dest)) {
        unlink($dest) or error("Failed to delete dest file");
    }
    zip_files($files, $dest);

    $fs = get_file_storage();

     $fileinfo = array(
        'contextid'   => $context->id,
        'component'   => 'mod_programming',
        'filearea'    => 'intro',
        'itemid'      => 0,
        'filepath'    => '/',
        'filename'    => $zipfilename
    );

    if ($fs->file_exists($fileinfo['contextid'], $fileinfo['component'], $fileinfo['filearea'], $fileinfo['itemid'], $fileinfo['filepath'], $fileinfo['filename'])) {

        $filepath = clean_param( $fileinfo['filepath'], PARAM_PATH);
        $filename = clean_param($fileinfo['filename'], PARAM_FILE);

        if ($filename === '') {
            $filename = '.';
        }

        $pathnamehash = $fs->get_pathname_hash($fileinfo['contextid'], $fileinfo['component'], $fileinfo['filearea'], $fileinfo['itemid'], $filepath, $filename);
        $sf = $fs->get_file_by_hash($pathnamehash);
        $sf->delete();
    }
    $storedfile = $fs->create_file_from_pathname($fileinfo, $dest);

    // remove temp
    fulldelete($dirname);

     $filelink = moodle_url::make_pluginfile_url($storedfile->get_contextid(), $storedfile->get_component(), $storedfile->get_filearea(), null ,$storedfile->get_filepath(), $storedfile->get_filename(), true);
    // $filelink = "http://127.0.0.1/moodle/pluginfile.php?forcedownload=1&file=/7972/mod_programming/intro/programming/test.zip";
    
    $referer = $_SERVER['HTTP_REFERER'];

/// Print the page header
    $PAGE->set_title($programming->name);
    $PAGE->set_heading(format_string($course->fullname));
    echo $OUTPUT->header();

/// Print tabs
    $renderer = $PAGE->get_renderer('mod_programming');
    $tabs = programming_navtab('reports', 'reports-packaging', $course, $programming, $cm);
    echo $renderer->render_navtab($tabs);

    echo html_writer::tag('h2', $programming->name);
    echo html_writer::tag('h3', get_string('packagesuccess', 'programming'));

    echo html_writer::tag('p', "<a href='$filelink'>".get_string('download', 'programming').'</a>');
    echo html_writer::tag('p', "<a href='$referer'>".get_string('return', 'programming').'</a>');

/// Finish the page
    echo $OUTPUT->footer($course);
