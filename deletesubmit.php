<?PHP  // $Id: view.php,v 1.1 2003/09/30 02:45:19 moodler Exp $

/// This page prints a particular instance of programming
/// (Replace programming with the name of your module)

    require_once('../../config.php');
    require_once('lib.php');

    $id = required_param('id', PARAM_INT);
    $submitid = optional_param_array('submitid', array(), PARAM_INT);
    $confirm = optional_param('confirm', 0, PARAM_INT);
    $href = optional_param('href', $_SERVER['HTTP_REFERER'], PARAM_URL);

    $PAGE->set_url('/mod/programming/deletesubmit.php');

    if ($id) {
        if (! $cm = get_coursemodule_from_id('programming', $id)) {
            print_error('Course Module ID was incorrect');
        }
    
        if (! $course = $DB->get_record('course', array('id' => $cm->course))) {
            print_error('Course is misconfigured');
        }
    
        if (! $programming = $DB->get_record('programming', array('id' => $cm->instance))) {
            print_error('Course module is incorrect');
        }
    }
    require_login($course->id, true, $cm);
    $context = get_context_instance(CONTEXT_MODULE, $cm->id);

    require_capability('mod/programming:deleteothersubmit', $context);

/// Print the page header
    $PAGE->set_title($programming->name);
    $PAGE->set_heading(format_string($course->fullname));
    echo $OUTPUT->header();

/// Print the main part of the page
    if ($confirm) {
        foreach ($submitid as $sid) {
            $submit = $DB->get_record('programming_submits', array('id' => $sid));
            if ($submit) programming_delete_submit($submit);
        }
        add_to_log($course->id, 'programming', 'delete submit', '', implode($submitid, ','));

        echo '<div class="maincontent generalbox">';
        echo '<p align="center">'.get_string('deleted').'</p>';
        echo '<p align="center"><a href="'.$href.'">'.get_string('continue').'</a></p>';
        echo '</div>';
    } else {
        echo '<table align="center" width="60%" class="noticebox" border="0" cellpadding="20" cellspacing="0">';
        echo '<tr><td bgcolor="#FFAAAA" class="noticeboxcontent">';
        echo '<h2 class="main">'.get_string('deletesubmitconfirm', 'programming').'</h2>';
        echo '<ul>';
        foreach ($submitid as $sid) {
            $submit = $DB->get_record('programming_submits', array('id' => $sid));
            $tm = userdate($submit->timemodified);
            $user = fullname($DB->get_record('user', array('id' => $submit->userid)));
            echo "<li>$sid $user $tm</li>";
        }
        echo '</ul>';
        echo '<form name="form" method="post">';
        foreach ($submitid as $sid) {
            echo "<input type='hidden' name='submitid[]' value='$sid' />";
        }
        echo "<input type='hidden' name='id' value='$id' />";
        echo '<input type="hidden" name="confirm" value="1" />';
        echo "<input type='hidden' name='href' value='$href' />";
        echo '<input type="submit" value=" '.get_string('yes').' " /> ';
        echo '<input type="button" value=" '.get_string('no').' " onclick="javascript:history.go(-1);" />';

        echo '</form>';
        echo '</td></tr></table>';
    }

/// Finish the page
    echo $OUTPUT->footer($course);

?>
