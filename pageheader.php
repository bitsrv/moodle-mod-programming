<?php
    $PAGE->requires->css = '/mod/programming/styles.css';

    $strprogrammings = get_string('modulenameplural', 'programming');
    $strprogramming  = get_string('modulename', 'programming');

    $meta = '';
    foreach ($CFG->scripts as $script) {
        //$meta .= '<script type="text/javascript" src="'.$script.'"></script>';
        //$meta .= "\n";
        $PAGE->requires->js($script);
    }

    if (isset($cm)) {
        $PAGE->navbar->add($pagename,$cm);
        // $navigation = build_navigation($pagename, $cm);
    } else {
        $PAGE->navbar->add($strprogrammings);
        // $navigation = build_navigation($strprogrammings);
    }

    $PAGE->set_title(empty($programming) ? $strprogrammings.' '.$title : $course->shortname.': '.$programming->name);
    $PAGE->set_heading(format_string($course->fullname));

    echo $OUTPUT->header();

/*
   print_header(
        empty($programming) ? $strprogrammings.' '.$title : $course->shortname.': '.$programming->name,
        $course->fullname,
        $navigation,
        '', // focus
        '',
        true,
        !empty($cm) ? update_module_button($cm->id, $course->id, $strprogramming) : '', 
        !empty($cm) ? navmenu($course, $cm) : navmenu($course));
     */
?>
